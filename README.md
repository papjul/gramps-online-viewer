# Gramps Online Viewer

Source code for an (unofficial) online viewer of a Gramps database, with responsive features to make it viewable on mobile.

It was made because I needed a way to show my family my works on genealogy during family meetings and it’s much more easier to have it directly available online!

It is not secure by design, due to usage of Python Pickle (unserializing is never safe) by original author of Gramps project, so you should NEVER trust SQLite database of any random user and only use it with databases YOU trust.

## Credits

Thanks to:
* Gramps team for the original project
* @Arkellys for their responsive tree in pure CSS

## Future features

* Log in (avoid letting anyone see your tree). This needs an additional SQLite database.
* Two-Factor Authentication (more secure)
* More details (particularly life events)
* Internationalization (only in French currently, though there is not much things written)
* Make it more responsive (tables should be shrinked on mobile)
* Avoid using an external file for Python calls (reduce installation steps)
* Multi-databases support
* Write tests
* Provide a sample database

## Installation

### Requirements
* Your Gramps database to be in SQLite format (v18 was used)
* Configure your web server according to https://symfony.com/doc/current/setup/web_server_configuration.html
* PHP 7.3 (though it probably works on lower versions)
* Python 3 (tested with 3.6). It is required only to unserialize data from the database.
* Probably other things (to be completed)

### Steps
* Run composer install
* Run yarn install && yarn encore dev

* Put your sqlite.db in var/ directory.
* Make sure var/pickled_data.txt can be written by PHP.
