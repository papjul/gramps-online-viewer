<?php

declare(strict_types=1);

/**
 * This file is part of Gramps Online Viewer.
 *
 * Gramps Online Viewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gramps Online Viewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Service\DatabaseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/families")
 */
class FamilyController extends AbstractController
{
    /**
     * Index of families.
     *
     * @Route("", name="families_index")
     */
    public function indexAction(DatabaseService $metadataRepository): Response
    {
        $families = $metadataRepository->findAllFamilies();

        return $this->render('families/index.html.twig', [
            'families' => $families,
        ]);
    }

    /**
     * Show details of a family.
     *
     * @Route("/{handle}", name="family_show", requirements={"handle": "[a-f\d]+"})
     */
    public function showAction(DatabaseService $metadataRepository, ?string $handle = null): Response
    {
        $family = $metadataRepository->findFamily($handle);
        $events = $metadataRepository->findFamilyEvents($family);

        return $this->render('families/show.html.twig', [
            'family' => $family,
            'events' => $events,
        ]);
    }
}
