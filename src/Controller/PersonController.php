<?php

declare(strict_types=1);

/**
 * This file is part of Gramps Online Viewer.
 *
 * Gramps Online Viewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gramps Online Viewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Service\DatabaseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/persons")
 */
class PersonController extends AbstractController
{
    /**
     * Index of persons.
     *
     * @Route("", name="persons_index")
     */
    public function indexAction(DatabaseService $metadataRepository): Response
    {
        $persons = $metadataRepository->findAllPersons();

        return $this->render('persons/index.html.twig', [
            'persons' => $persons,
        ]);
    }

    /**
     * Show details of a person.
     *
     * @Route("/{handle}", name="person_show", requirements={"handle": "[a-f\d]+"})
     */
    public function showAction(DatabaseService $metadataRepository, ?string $handle = null): Response
    {
        $person = $metadataRepository->findPerson($handle);
        $events = $metadataRepository->findPersonEvents($person);

        return $this->render('persons/show.html.twig', [
            'person' => $person,
            'events' => $events,
        ]);
    }
}
