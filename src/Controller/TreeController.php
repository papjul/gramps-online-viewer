<?php

declare(strict_types=1);

/**
 * This file is part of Gramps Online Viewer.
 *
 * Gramps Online Viewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gramps Online Viewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */
namespace App\Controller;

use App\Service\DatabaseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tree")
 */
class TreeController extends AbstractController
{
    /**
     * Tree index.
     *
     * @Route("", name="tree_index")
     */
    public function indexAction(DatabaseService $metadataRepository): Response
    {
        return $this->renderTree($metadataRepository);
    }

    /**
     * Homepage controller.
     *
     * @Route("/{handle}", name="tree_show", requirements={"handle": "[a-f\d]+"})
     */
    public function showTreeAction(DatabaseService $metadataRepository, ?string $handle = null): Response
    {
        return $this->renderTree($metadataRepository, $handle);
    }

    private function renderTree(DatabaseService $metadataRepository, ?string $handle = null): Response
    {
        if (null === $handle) {
            $handle = $metadataRepository->findDefaultPersonHandle();
        }
        $defaultPerson = $metadataRepository->findPerson($handle);
        if (null !== $defaultPerson && count($defaultPerson['data'][9])) {
            $defaultPersonFamily = $metadataRepository->findFamily($defaultPerson['data'][9][0]);

            $fatherPerson = ($defaultPersonFamily['father_handle'] !== null) ? $metadataRepository->findPerson($defaultPersonFamily['father_handle']) : null;
            if (null !== $fatherPerson && count($fatherPerson['data'][9])) {
                $fatherPersonFamily = $metadataRepository->findFamily($fatherPerson['data'][9][0]);
                $fGdFatherPerson = ($fatherPersonFamily['father_handle'] !== null) ? $metadataRepository->findPerson($fatherPersonFamily['father_handle']) : null;
                $fGdMotherPerson = ($fatherPersonFamily['mother_handle'] !== null) ? $metadataRepository->findPerson($fatherPersonFamily['mother_handle']) : null;
            } else {
                $fGdFatherPerson = null;
                $fGdMotherPerson = null;
            }

            $motherPerson = ($defaultPersonFamily['mother_handle'] !== null) ? $metadataRepository->findPerson($defaultPersonFamily['mother_handle']) : null;
            if (null !== $motherPerson && count($motherPerson['data'][9])) {
                $motherPersonFamily = $metadataRepository->findFamily($motherPerson['data'][9][0]);
                $mGdFatherPerson = ($motherPersonFamily['father_handle'] !== null) ? $metadataRepository->findPerson($motherPersonFamily['father_handle']) : null;
                $mGdMotherPerson = ($motherPersonFamily['mother_handle'] !== null) ? $metadataRepository->findPerson($motherPersonFamily['mother_handle']) : null;
            } else {
                $mGdFatherPerson = null;
                $mGdMotherPerson = null;
            }
        } else {
            $defaultPersonFamily = null;
            $fatherPerson = null;
            $motherPerson = null;
            $fGdFatherPerson = null;
            $fGdMotherPerson = null;
            $mGdFatherPerson = null;
            $mGdMotherPerson = null;
        }

        $childPersons = [];
        $spousePerson = null;
        if (count($defaultPerson['data'][8])) {
            $childFamily = $metadataRepository->findFamily($defaultPerson['data'][8][0]);
            if($childFamily['father_handle'] !== null && $childFamily['father_handle'] !== $defaultPerson['handle']) {
                $spousePerson = $metadataRepository->findPerson($childFamily['father_handle']);
            }
            if($childFamily['mother_handle'] !== null && $childFamily['mother_handle'] !== $defaultPerson['handle']) {
                $spousePerson = $metadataRepository->findPerson($childFamily['mother_handle']);
            }
            foreach($childFamily['data'][4] as $child) {
                $childPersons[] = $metadataRepository->findPerson($child[3]);
            }
        }

        return $this->render('tree/index.html.twig', [
            'defaultPerson' => $defaultPerson,
            'fatherPerson' => $fatherPerson,
            'motherPerson' => $motherPerson,
            'fGdFatherPerson' => $fGdFatherPerson,
            'fGdMotherPerson' => $fGdMotherPerson,
            'mGdFatherPerson' => $mGdFatherPerson,
            'mGdMotherPerson' => $mGdMotherPerson,
            'spousePerson' => $spousePerson,
            'childPersons' => $childPersons
        ]);
    }
}