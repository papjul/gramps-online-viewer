<?php

declare(strict_types=1);

/**
 * This file is part of Gramps Online Viewer.
 *
 * Gramps Online Viewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gramps Online Viewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Service;

use Doctrine\DBAL\Connection;
use Symfony\Component\HttpKernel\KernelInterface;

class DatabaseService
{
    /**
     * @var Connection
     */
    private $conn;
    /**
     * @var KernelInterface
     */
    private $appKernel;

    public function __construct(KernelInterface $appKernel, Connection $conn)
    {
        $this->conn = $conn;
        $this->appKernel = $appKernel;
    }

    private function unserialize($str)
    {
        file_put_contents($this->appKernel->getProjectDir().'/var/pickled_data.txt', $str);
        exec('python '.$this->appKernel->getProjectDir().'/bin/pickle2json.py -p '.$this->appKernel->getProjectDir().'/var/pickled_data.txt', $json_data);

        return json_decode($json_data[0]);
    }

    private function convertEventType(int $type_id)
    {
        // TODO: Complete with https://github.com/gramps-project/gramps/blob/master/gramps/gen/lib/eventtype.py
        // And https://raw.githubusercontent.com/gramps-project/gramps/master/po/fr.po
        switch ($type_id) {
            case 1:
                return 'Mariage';
                break;

            case 7:
                return 'Divorce';
                break;

            case 12:
                return 'Naissance';
                break;

            case 13:
                return 'Décès';
                break;

            case 15:
                return 'Baptême';
                break;

            case 19:
                return 'Inhumation';
                break;

            default:
                return $type_id;
        }
    }

    public function findDefaultPersonHandle()
    {
        $sql = 'SELECT * FROM metadata WHERE setting = ?';
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['default-person-handle']);
        $result = $stmt->fetch();

        return $this->unserialize($result['value']);
    }

    public function findPerson(string $handle)
    {
        $sql = 'SELECT * FROM person WHERE handle = ?';
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([$handle]);
        $result = $stmt->fetch();
        $result['data'] = $this->unserialize($result['blob_data']);

        return $result;
    }

    public function findFamily(string $handle)
    {
        $sql = 'SELECT family.*, father.surname AS father_surname, father.given_name AS father_given_name, mother.surname AS mother_surname, mother.given_name AS mother_given_name
                FROM   family
                LEFT JOIN person father ON father.handle = family.father_handle
                LEFT JOIN person mother ON mother.handle = family.mother_handle
                WHERE family.handle = ?';
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([$handle]);
        $result = $stmt->fetch();
        $result['data'] = $this->unserialize($result['blob_data']);

        return $result;
    }

    public function findAllPersons($deblob = false)
    {
        $sql = 'SELECT * FROM person';
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll();
        if ($deblob) {
            foreach ($results as $result) {
                $result['data'] = $this->unserialize($result['blob_data']);
            }
        }

        return $results;
    }

    public function findAllFamilies($deblob = false)
    {
        $sql = 'SELECT family.*, father.surname AS father_surname, father.given_name AS father_given_name, mother.surname AS mother_surname, mother.given_name AS mother_given_name
                FROM   family
                LEFT JOIN person father ON father.handle = family.father_handle
                LEFT JOIN person mother ON mother.handle = family.mother_handle';
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll();
        if ($deblob) {
            foreach ($results as $result) {
                $result['data'] = $this->unserialize($result['blob_data']);
            }
        }

        return $results;
    }

    public function findEvent(string $handle)
    {
        $sql = 'SELECT *
                FROM   event
                WHERE  handle = ?';
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([$handle]);
        $result = $stmt->fetch();
        $result['data'] = $this->unserialize($result['blob_data']);

        $result['type'] = $this->convertEventType($result['data'][2][0]);
        $result['date'] = $result['data'][3][3][0].'/'.$result['data'][3][3][1].'/'.$result['data'][3][3][2];
        if ('0/0/0' === $result['date']) {
            $result['date'] = $result['data'][3][4];
        }

        return $result;
    }

    public function findPersonEvents($person)
    {
        $results = [];
        if (count($person['data'][7])) {
            foreach ($person['data'][7] as $event) {
                $results[] = $this->findEvent($event[3]);
            }
        }

        return $results;
    }

    public function findFamilyEvents($family)
    {
        $results = [];
        if (count($family['data'][6])) {
            foreach ($family['data'][6] as $event) {
                $results[] = $this->findEvent($event[3]);
            }
        }

        return $results;
    }
}
